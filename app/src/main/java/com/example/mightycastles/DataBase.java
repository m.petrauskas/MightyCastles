package com.example.mightycastles;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import com.example.mightycastles.mightycastles.classes.SQLiteTest;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DataBase extends AppCompatActivity {

    private TextView dbview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_base);
        dbview = findViewById(R.id.dbtext);
        SQLiteTest test = new SQLiteTest();
        ResultSet rs;
        try {
            rs = test.displayUsers();
            while(rs.next()){
                dbview.setText(rs.getString("fname") + " " + rs.getString("lname"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
