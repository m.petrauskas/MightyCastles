package com.example.mightycastles;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mightycastles.mightycastles.classes.Card;
import com.example.mightycastles.mightycastles.classes.Castle;
import com.example.mightycastles.mightycastles.classes.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameplayWithBots extends Activity {
    //buttons
    private Button handButton;
    private Button lastButton;
    //private ImageButton player1DeckButtonVAR;
    //private ImageButton player2DeckButtonVAR;
    private Button toggleTurnButton;
    //Turn
    private boolean player1Turn;
    private boolean intermissionTurn;
    private boolean pauseTime;
    //Card buttons
    private ImageButton cardButton1;
    private ImageButton  cardButton2;
    private ImageButton  cardButton3;
    private ImageButton  cardButton4;
    private ImageButton  cardButton5;
    //Images
    private ImageView castleImage;
    private ImageView lastCard1;
    private ImageView lastCard2;
    private ImageView winScreen;
    //show
    private boolean showMyHand;
    private boolean showLastCard;
    private boolean cardUsed;
    //textView
    private TextView resourceText;
    private TextView turnText;
    //players
    private Player player1;
    private Player player2;
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MainMeniu.ring.stop();
        MainMeniu.ring = MediaPlayer.create(GameplayWithBots.this,R.raw.gameplay_theme);
        MainMeniu.ring.setLooping(true);
        MainMeniu.ring.start();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gameplay_with_bots);
        this.pauseTime=false;
        this.cardUsed=false;
        showMyHand=false;
        player1Turn=false;
        //Card List creation
        List<Card> cardCollection = new ArrayList<>();
        cardCollection.add( new Card (0, "Castle", 0, 18, 0, 20, "Castle", "CastleCard"));
        cardCollection.add( new Card (1, "Drake", 25, 0, 0, -21, "Enemy_castle", "DrakeCard"));
        cardCollection.add( new Card (2, "Thief", 0, 0, 15, 5, "All_resources", "ThiefCard"));
        cardCollection.add( new Card (3, "Wall", 0, 12, 0, 22, "Wall", "WallCard"));
        cardCollection.add( new Card (4, "Warrior", 0, 0, 5, -4, "Enemy_castle", "WarriorCard"));
        cardCollection.add( new Card (5, "Wizard", 20, 0, 0, -15, "Enemy_castle", "WizardCard"));
        //difficulty
        String difficulty_id;
        difficulty_id = Play.difficulty;

        //Player 1
        Castle player1Castle = new Castle(
                1,
                Play.player1Nametext,
                30,
                10,
                10,
                5,
                10,
                5,
                10,
                5
        );
        ArrayList<Card> player1Deck=new ArrayList<>();
        ArrayList<Card> player1Hand=new ArrayList<>();
        player1Deck.add(cardCollection.get(4));
        player1Deck.add(cardCollection.get(4));
        player1Deck.add(cardCollection.get(4));
        player1Deck.add(cardCollection.get(4));
        player1Deck.add(cardCollection.get(4));
        player1Deck.add(cardCollection.get(4));
        player1Deck.add(cardCollection.get(4));
        player1Deck.add(cardCollection.get(4));
        player1Deck.add(cardCollection.get(0));
        player1Deck.add(cardCollection.get(0));
        player1Deck.add(cardCollection.get(0));
        player1Deck.add(cardCollection.get(0));
        player1Deck.add(cardCollection.get(0));
        player1Deck.add(cardCollection.get(0));
        player1Deck.add(cardCollection.get(0));
        player1Deck.add(cardCollection.get(0));
        player1Deck.add(cardCollection.get(1));
        player1Deck.add(cardCollection.get(1));
        player1Deck.add(cardCollection.get(5));
        player1Deck.add(cardCollection.get(5));
        player1Deck.add(cardCollection.get(2));
        player1Deck.add(cardCollection.get(2));
        player1Deck.add(cardCollection.get(2));
        player1Deck.add(cardCollection.get(4));
        player1Deck.add(cardCollection.get(3));
        player1Deck.add(cardCollection.get(3));
        player1Deck.add(cardCollection.get(3));
        player1Deck.add(cardCollection.get(3));
        player1Deck.add(cardCollection.get(3));
        player1Deck.add(cardCollection.get(3));

        this.player1 = new Player(0, player1Hand, player1Deck, player1Castle);
        //Player  2
        Castle player2Castle;
        if(difficulty_id == "Easy"){

            player2Castle = new Castle(
                    2,
                    Play.player2Nametext,
                    20,
                    5,
                    5,
                    5,
                    5,
                    5,
                    5,
                    5
            );
        }

        else if(difficulty_id == "Medium"){

            player2Castle = new Castle(
                    2,
                    Play.player2Nametext,
                    30,
                    10,
                    10,
                    5,
                    10,
                    5,
                    10,
                    5
            );
        }

        else if(difficulty_id == "Hard"){

            player2Castle = new Castle(
                    2,
                    Play.player2Nametext,
                    40,
                    25,
                    15,
                    5,
                    15,
                    5,
                    15,
                    5
            );

        }
        else{
            player2Castle = new Castle(
                    2,
                    "name2",
                    25,
                    0,
                    10,
                    5,
                    10,
                    5,
                    10,
                    5
            );
        }


        ArrayList<Card> player2Deck = new ArrayList<>();
        ArrayList<Card> player2Hand = new ArrayList<>();
        player2Deck.add(cardCollection.get(4));
        player2Deck.add(cardCollection.get(4));
        player2Deck.add(cardCollection.get(4));
        player2Deck.add(cardCollection.get(4));
        player2Deck.add(cardCollection.get(4));
        player2Deck.add(cardCollection.get(4));
        player2Deck.add(cardCollection.get(4));
        player2Deck.add(cardCollection.get(4));
        player2Deck.add(cardCollection.get(0));
        player2Deck.add(cardCollection.get(0));
        player2Deck.add(cardCollection.get(0));
        player2Deck.add(cardCollection.get(0));
        player2Deck.add(cardCollection.get(0));
        player2Deck.add(cardCollection.get(0));
        player2Deck.add(cardCollection.get(0));
        player2Deck.add(cardCollection.get(0));
        player2Deck.add(cardCollection.get(1));
        player2Deck.add(cardCollection.get(1));
        player2Deck.add(cardCollection.get(5));
        player2Deck.add(cardCollection.get(5));
        player2Deck.add(cardCollection.get(2));
        player2Deck.add(cardCollection.get(2));
        player2Deck.add(cardCollection.get(2));
        player2Deck.add(cardCollection.get(4));
        player2Deck.add(cardCollection.get(3));
        player2Deck.add(cardCollection.get(3));
        player2Deck.add(cardCollection.get(3));
        player2Deck.add(cardCollection.get(3));
        player2Deck.add(cardCollection.get(3));
        player2Deck.add(cardCollection.get(3));


        this.player2 = new Player(1, player2Hand, player2Deck, player2Castle);
        refreshResources();
        //Button declaration
        this.winScreen=findViewById(R.id.victoryScreen);
        this.lastCard1=findViewById(R.id.card1Last);
        this.lastCard2=findViewById(R.id.card2Last);
        this.cardButton1 = findViewById(R.id.card1Button);
        this.cardButton2 = findViewById(R.id.card2Button);
        this.cardButton3 = findViewById(R.id.card3Button);
        this.cardButton4 = findViewById(R.id.card4Button);
        this.cardButton5 = findViewById(R.id.card5Button);
        this.toggleTurnButton = findViewById(R.id.turnToggle);
        //Buttons
        this.lastButton = findViewById(R.id.showLast);
        this.lastButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!intermissionTurn&&!pauseTime&&player1Turn) {
                    showLastCardToggle();
                }
            }
        });

        this.handButton = findViewById(R.id.showHand);
        this.handButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!intermissionTurn&&!pauseTime&&player1Turn) {
                    showCardsToggle();
                }
            }
        });
        /*
        this.player1DeckButtonVAR = findViewById(R.id.player1DeckButton);
        this.player1DeckButtonVAR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playerDraw(player1);
            }
        });

        this.player2DeckButtonVAR = findViewById(R.id.player2DeckButton);
        this.player2DeckButtonVAR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playerDraw(player2);
            }
        });
        */
        this.cardButton1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(!intermissionTurn&&!pauseTime&&player1Turn){
                    useCardButton1();
                }
            }
        });
        this.cardButton2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(!intermissionTurn&&!pauseTime&&player1Turn) {
                    useCardButton2();
                }
            }
        });
        this.cardButton3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(!intermissionTurn&&!pauseTime&&player1Turn) {
                    useCardButton3();
                }
            }
        });
        this.cardButton4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(!intermissionTurn&&!pauseTime&&player1Turn) {
                    useCardButton4();
                }
            }
        });
        this.cardButton5.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(!intermissionTurn&&!pauseTime&&player1Turn) {
                    useCardButton5();
                }
            }
        });
        this.toggleTurnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!intermissionTurn&&!pauseTime&&player1Turn) {
                    intermission();
                }

            }
        });
        intermission();

        for (int i=0; i<3; i++){
            this.player1Turn = true;
            playerDraw();
            this.player1Turn = false;
            playerDraw();
        }

    }
    public void showCardsToggle(){
        if (this.showMyHand){
            refreshHandPlayer();
            this.showMyHand=false;
        }
        else{
            findViewById(R.id.card1Button).setVisibility(View.GONE);
            findViewById(R.id.card2Button).setVisibility(View.GONE);
            findViewById(R.id.card3Button).setVisibility(View.GONE);
            findViewById(R.id.card4Button).setVisibility(View.GONE);
            findViewById(R.id.card5Button).setVisibility(View.GONE);
            this.showMyHand=true;
        }
    }
    public void showLastCardToggle(){
        if (this.showLastCard){
            findViewById(R.id.card1Last).setVisibility(View.VISIBLE);
            findViewById(R.id.card2Last).setVisibility(View.VISIBLE);
            this.showLastCard=false;
        }
        else{
            findViewById(R.id.card1Last).setVisibility(View.GONE);
            findViewById(R.id.card2Last).setVisibility(View.GONE);
            this.showLastCard=true;
        }
    }
    public void playerDraw(){
        if(this.player1Turn){
            Random r = new Random();
            int rMax = this.player1.getCardsInDeck().size() - 1;
            int randomNumber = r.nextInt((rMax)+1);

            if (rMax >= 0 && this.player1.getCardsInHand().size()<5 && this.player1.getCardsInDeck().size()>=1){
                this.player1.getCardsInHand().add(this.player1.getCardsInDeck().get(randomNumber));
                this.player1.getCardsInDeck().remove(randomNumber);
            }
        }
        else{
            Random r = new Random();
            int rMax = this.player2.getCardsInDeck().size() - 1;
            int randomNumber = r.nextInt((rMax)+1);

            if (rMax >= 0 && this.player2.getCardsInHand().size()<5 && this.player2.getCardsInDeck().size()>=1){
                this.player2.getCardsInHand().add(this.player2.getCardsInDeck().get(randomNumber));
                this.player2.getCardsInDeck().remove(randomNumber);
            }
        }
    }


    public void refreshHandPlayer(){
        if(this.player1Turn){
            if(this.player1.getCardsInHand().size()>=1){
                this.cardButton1.setImageResource(findPictureId(this.player1.getCardsInHand().get(0).getPictureName()));
                this.cardButton1.setVisibility(View.VISIBLE);
                if(this.player1.getCardsInHand().size()>=2){
                    this.cardButton2.setImageResource(findPictureId(this.player1.getCardsInHand().get(1).getPictureName()));
                    this.cardButton2.setVisibility(View.VISIBLE);
                    if(this.player1.getCardsInHand().size()>=3){
                        this.cardButton3.setImageResource(findPictureId(this.player1.getCardsInHand().get(2).getPictureName()));
                        this.cardButton3.setVisibility(View.VISIBLE);
                        if(this.player1.getCardsInHand().size()>=4){
                            this.cardButton4.setImageResource(findPictureId(this.player1.getCardsInHand().get(3).getPictureName()));
                            this.cardButton4.setVisibility(View.VISIBLE);
                            if(this.player1.getCardsInHand().size()>=5){
                                this.cardButton5.setImageResource(findPictureId(this.player1.getCardsInHand().get(4).getPictureName()));
                                this.cardButton5.setVisibility(View.VISIBLE);

                            }
                            else {
                                this.cardButton5.setVisibility(View.GONE);
                            }
                        }
                        else {
                            this.cardButton4.setVisibility(View.GONE);
                            this.cardButton5.setVisibility(View.GONE);
                        }
                    }
                    else {
                        this.cardButton3.setVisibility(View.GONE);
                        this.cardButton4.setVisibility(View.GONE);
                        this.cardButton5.setVisibility(View.GONE);
                    }
                }
                else {
                    this.cardButton2.setVisibility(View.GONE);
                    this.cardButton3.setVisibility(View.GONE);
                    this.cardButton4.setVisibility(View.GONE);
                    this.cardButton5.setVisibility(View.GONE);
                }
            }
            else{
                this.cardButton1.setVisibility(View.GONE);
                this.cardButton2.setVisibility(View.GONE);
                this.cardButton3.setVisibility(View.GONE);
                this.cardButton4.setVisibility(View.GONE);
                this.cardButton5.setVisibility(View.GONE);
            }
        }
        else{
            if(this.player2.getCardsInHand().size()>=1){
                this.cardButton1.setImageResource(R.drawable.card_back);
                this.cardButton1.setVisibility(View.VISIBLE);
                if(this.player2.getCardsInHand().size()>=2){
                    this.cardButton2.setImageResource(R.drawable.card_back);
                    this.cardButton2.setVisibility(View.VISIBLE);
                    if(this.player2.getCardsInHand().size()>=3){
                        this.cardButton3.setImageResource(R.drawable.card_back);
                        this.cardButton3.setVisibility(View.VISIBLE);
                        if(this.player2.getCardsInHand().size()>=4){
                            this.cardButton4.setImageResource(R.drawable.card_back);
                            this.cardButton4.setVisibility(View.VISIBLE);
                            if(this.player2.getCardsInHand().size()>=5){
                                this.cardButton5.setImageResource(R.drawable.card_back);
                                this.cardButton5.setVisibility(View.VISIBLE);

                            }
                            else {
                                this.cardButton5.setVisibility(View.GONE);
                            }
                        }
                        else {
                            this.cardButton4.setVisibility(View.GONE);
                            this.cardButton5.setVisibility(View.GONE);
                        }
                    }
                    else {
                        this.cardButton3.setVisibility(View.GONE);
                        this.cardButton4.setVisibility(View.GONE);
                        this.cardButton5.setVisibility(View.GONE);
                    }
                }
                else {
                    this.cardButton2.setVisibility(View.GONE);
                    this.cardButton3.setVisibility(View.GONE);
                    this.cardButton4.setVisibility(View.GONE);
                    this.cardButton5.setVisibility(View.GONE);
                }
            }
            else{
                this.cardButton1.setVisibility(View.GONE);
                this.cardButton2.setVisibility(View.GONE);
                this.cardButton3.setVisibility(View.GONE);
                this.cardButton4.setVisibility(View.GONE);
                this.cardButton5.setVisibility(View.GONE);
            }
        }

    }
    public int findPictureId(String imageName){
        switch (imageName){
            case "CastleCard":
                return R.drawable.card_castle;
            case "DrakeCard":
                return R.drawable.card_drake;
            case "ThiefCard":
                return R.drawable.card_thief;
            case "WallCard":
                return R.drawable.card_wall;
            case "WarriorCard":
                return R.drawable.card_warrior;
            case "WizardCard":
                return R.drawable.card_wizard;
        }
        return R.drawable.icon_castle;
    }
    public void refreshResources(){
        //refresh player1
        this.resourceText=findViewById(R.id.player1Bricks);
        this.resourceText.setText(Integer.toString(this.player1.getPlayerCastle().getBricks()));
        this.resourceText=findViewById(R.id.player1Worker);
        this.resourceText.setText(Integer.toString(this.player1.getPlayerCastle().getBuilders()));
        this.resourceText=findViewById(R.id.player1Mana);
        this.resourceText.setText(Integer.toString(this.player1.getPlayerCastle().getMana()));
        this.resourceText=findViewById(R.id.player1Magic);
        this.resourceText.setText(Integer.toString(this.player1.getPlayerCastle().getMagic()));
        this.resourceText=findViewById(R.id.player1Weapons);
        this.resourceText.setText(Integer.toString(this.player1.getPlayerCastle().getWeapons()));
        this.resourceText=findViewById(R.id.player1Blacksmith);
        this.resourceText.setText(Integer.toString(this.player1.getPlayerCastle().getBlacksmith()));
        this.resourceText=findViewById(R.id.player1HealthTxt);
        this.resourceText.setText(Integer.toString(this.player1.getPlayerCastle().getCastleHealth()));
        this.resourceText=findViewById(R.id.player1WallTxt);
        this.resourceText.setText(Integer.toString(this.player1.getPlayerCastle().getWallHealth()));
        //castle 1
        if(this.player1.getPlayerCastle().getCastleHealth()>=90){
            this.castleImage=findViewById(R.id.player1Castle);
            if(Play.player1Chroma=="FirstChroma"){
                this.castleImage.setImageResource(R.drawable.item_castle);
            }
            else if(Play.player1Chroma=="SecondChroma"){
                this.castleImage.setImageResource(R.drawable.item_castle_chroma2);
            }
            else{
                this.castleImage.setImageResource(R.drawable.item_castle_chroma1);
            }

        }
        else if(this.player1.getPlayerCastle().getCastleHealth()>=75){
            this.castleImage=findViewById(R.id.player1Castle);
            if(Play.player1Chroma=="FirstChroma"){
                this.castleImage.setImageResource(R.drawable.item_castle100_75);
            }
            else if(Play.player1Chroma=="SecondChroma"){
                this.castleImage.setImageResource(R.drawable.item_castle100_75_chroma2);
            }
            else{
                this.castleImage.setImageResource(R.drawable.item_castle100_75_chroma1);
            }

        }
        else if(this.player1.getPlayerCastle().getCastleHealth()>=50){
            this.castleImage=findViewById(R.id.player1Castle);
            if(Play.player1Chroma=="FirstChroma"){
                this.castleImage.setImageResource(R.drawable.item_castle75_50);
            }
            else if(Play.player1Chroma=="SecondChroma"){
                this.castleImage.setImageResource(R.drawable.item_castle75_50_chroma2);
            }
            else{
                this.castleImage.setImageResource(R.drawable.item_castle75_50_chroma1);
            }

        }
        else if(this.player1.getPlayerCastle().getCastleHealth()>=25){
            this.castleImage=findViewById(R.id.player1Castle);
            if(Play.player1Chroma=="FirstChroma"){
                this.castleImage.setImageResource(R.drawable.item_castle50_25);
            }
            else if(Play.player1Chroma=="SecondChroma"){
                this.castleImage.setImageResource(R.drawable.item_castle50_25_chroma2);
            }
            else{
                this.castleImage.setImageResource(R.drawable.item_castle50_25_chroma1);
            }

        }
        else if(this.player1.getPlayerCastle().getCastleHealth()>0){
            this.castleImage=findViewById(R.id.player1Castle);
            if(Play.player1Chroma=="FirstChroma"){
                this.castleImage.setImageResource(R.drawable.item_castle25_0);
            }
            else if(Play.player1Chroma=="SecondChroma"){
                this.castleImage.setImageResource(R.drawable.item_castle25_0_chroma2);
            }
            else{
                this.castleImage.setImageResource(R.drawable.item_castle25_0_chroma1);
            }

        }
        else{
            this.castleImage=findViewById(R.id.player1Castle);
            if(Play.player1Chroma=="FirstChroma"){
                this.castleImage.setImageResource(R.drawable.icon_castle);
            }
            else if(Play.player1Chroma=="SecondChroma"){
                this.castleImage.setImageResource(R.drawable.icon_castle);
            }
            else{
                this.castleImage.setImageResource(R.drawable.icon_castle);
            }

        }
        //wall 1
        if(this.player1.getPlayerCastle().getWallHealth()>=90){
            this.castleImage=findViewById(R.id.player1Wall);
            this.castleImage.setImageResource(R.drawable.item_wall);
        }
        else if(this.player1.getPlayerCastle().getWallHealth()>=75){
            this.castleImage=findViewById(R.id.player1Wall);
            this.castleImage.setImageResource(R.drawable.item_wall100_75);
        }
        else if(this.player1.getPlayerCastle().getWallHealth()>=50){
            this.castleImage=findViewById(R.id.player1Wall);
            this.castleImage.setImageResource(R.drawable.item_wall75_50);
        }
        else if(this.player1.getPlayerCastle().getWallHealth()>=25){
            this.castleImage=findViewById(R.id.player1Wall);
            this.castleImage.setImageResource(R.drawable.item_wall25_0);
        }
        else if(this.player1.getPlayerCastle().getWallHealth()>0){
            this.castleImage=findViewById(R.id.player1Wall);
            this.castleImage.setImageResource(R.drawable.item_wall);
        }
        else{
            this.castleImage=findViewById(R.id.player1Wall);
            this.castleImage.setImageResource(R.drawable.item_wall);
        }
        //refresh player2
        this.resourceText=findViewById(R.id.player2Bricks);
        this.resourceText.setText(Integer.toString(this.player2.getPlayerCastle().getBricks()));
        this.resourceText=findViewById(R.id.player2Worker);
        this.resourceText.setText(Integer.toString(this.player2.getPlayerCastle().getBuilders()));
        this.resourceText=findViewById(R.id.player2Mana);
        this.resourceText.setText(Integer.toString(this.player2.getPlayerCastle().getMana()));
        this.resourceText=findViewById(R.id.player2Magic);
        this.resourceText.setText(Integer.toString(this.player2.getPlayerCastle().getMagic()));
        this.resourceText=findViewById(R.id.player2Weapons);
        this.resourceText.setText(Integer.toString(this.player2.getPlayerCastle().getWeapons()));
        this.resourceText=findViewById(R.id.player2Blacksmith);
        this.resourceText.setText(Integer.toString(this.player2.getPlayerCastle().getBlacksmith()));
        this.resourceText=findViewById(R.id.player2HealthTxt);
        this.resourceText.setText(Integer.toString(this.player2.getPlayerCastle().getCastleHealth()));
        this.resourceText=findViewById(R.id.player2WallTxt);///
        this.resourceText.setText(Integer.toString(this.player2.getPlayerCastle().getWallHealth()));
        //castle 2
        if(this.player2.getPlayerCastle().getCastleHealth()>=90){
            this.castleImage=findViewById(R.id.player2Castle);
            if(Play.player2Chroma=="FirstChroma"){
                this.castleImage.setImageResource(R.drawable.item_castle);
            }
            else if(Play.player2Chroma=="SecondChroma"){
                this.castleImage.setImageResource(R.drawable.item_castle_chroma3);
            }
            else{
                this.castleImage.setImageResource(R.drawable.item_castle_chroma1);
            }
        }
        else if(this.player2.getPlayerCastle().getCastleHealth()>=75){
            this.castleImage=findViewById(R.id.player2Castle);
            if(Play.player2Chroma=="FirstChroma"){
                this.castleImage.setImageResource(R.drawable.item_castle100_75);
            }
            else if(Play.player2Chroma=="SecondChroma"){
                this.castleImage.setImageResource(R.drawable.item_castle100_75_chroma3);
            }
            else{
                this.castleImage.setImageResource(R.drawable.item_castle100_75_chroma1);
            }
        }
        else if(this.player2.getPlayerCastle().getCastleHealth()>=50){
            this.castleImage=findViewById(R.id.player2Castle);
            if(Play.player2Chroma=="FirstChroma"){
                this.castleImage.setImageResource(R.drawable.item_castle75_50);
            }
            else if(Play.player2Chroma=="SecondChroma"){
                this.castleImage.setImageResource(R.drawable.item_castle75_50_chroma3);
            }
            else{
                this.castleImage.setImageResource(R.drawable.item_castle75_50_chroma1);
            }
        }
        else if(this.player2.getPlayerCastle().getCastleHealth()>=25){
            this.castleImage=findViewById(R.id.player2Castle);
            if(Play.player2Chroma=="FirstChroma"){
                this.castleImage.setImageResource(R.drawable.item_castle50_25);
            }
            else if(Play.player2Chroma=="SecondChroma"){
                this.castleImage.setImageResource(R.drawable.item_castle50_25_chroma3);
            }
            else{
                this.castleImage.setImageResource(R.drawable.item_castle50_25_chroma1);
            }
        }
        else if(this.player2.getPlayerCastle().getCastleHealth()>0){
            this.castleImage=findViewById(R.id.player2Castle);
            if(Play.player2Chroma=="FirstChroma"){
                this.castleImage.setImageResource(R.drawable.item_castle25_0);
            }
            else if(Play.player2Chroma=="SecondChroma"){
                this.castleImage.setImageResource(R.drawable.item_castle25_0_chroma3);
            }
            else{
                this.castleImage.setImageResource(R.drawable.item_castle25_0_chroma1);
            }
        }
        else{
            this.castleImage=findViewById(R.id.player2Castle);
            if(Play.player2Chroma=="FirstChroma"){
                this.castleImage.setImageResource(R.drawable.icon_castle);
            }
            else if(Play.player2Chroma=="SecondChroma"){
                this.castleImage.setImageResource(R.drawable.icon_castle);
            }
            else{
                this.castleImage.setImageResource(R.drawable.icon_castle);
            }
        }
        //wall 2
        if(this.player2.getPlayerCastle().getWallHealth()>=90){
            this.castleImage=findViewById(R.id.player2Wall);
            this.castleImage.setImageResource(R.drawable.item_wall);
        }
        else if(this.player2.getPlayerCastle().getWallHealth()>=75){
            this.castleImage=findViewById(R.id.player2Wall);
            this.castleImage.setImageResource(R.drawable.item_wall100_75);
        }
        else if(this.player2.getPlayerCastle().getWallHealth()>=50){
            this.castleImage=findViewById(R.id.player2Wall);
            this.castleImage.setImageResource(R.drawable.item_wall75_50);
        }
        else if(this.player2.getPlayerCastle().getWallHealth()>=25){
            this.castleImage=findViewById(R.id.player2Wall);
            this.castleImage.setImageResource(R.drawable.item_wall25_0);
        }
        else if(this.player2.getPlayerCastle().getWallHealth()>0){
            this.castleImage=findViewById(R.id.player2Wall);
            this.castleImage.setImageResource(R.drawable.item_wall);
        }
        else{
            this.castleImage=findViewById(R.id.player2Wall);
            this.castleImage.setImageResource(R.drawable.item_wall);
        }
    }

    public void useCardButton1(){
        if (this.player1Turn) {
            if (this.player1.getCardsInHand().size() >= 1) {
                if(this.player1.getCardsInHand().get(0).getPrice_bricks()<=this.player1.getPlayerCastle().getBricks() &&
                        this.player1.getCardsInHand().get(0).getPrice_mana()<=this.player1.getPlayerCastle().getMana() &&
                        this.player1.getCardsInHand().get(0).getPrice_weapons()<=this.player1.getPlayerCastle().getWeapons())
                {
                    cardUse(this.player1.getCardsInHand().get(0));
                    this.player1.getPlayerCastle().addBricks(this.player1.getCardsInHand().get(0).getPrice_bricks() * -1);
                    this.player1.getPlayerCastle().addMana(this.player1.getCardsInHand().get(0).getPrice_mana() * -1);
                    this.player1.getPlayerCastle().addWeapons(this.player1.getCardsInHand().get(0).getPrice_weapons() * -1);
                    this.lastCard1.setImageResource(findPictureId(this.player1.getCardsInHand().get(0).getPictureName()));
                    this.player1.getCardsInHand().remove(0);
                    refreshResources();
                    updateWin();
                    if(!this.pauseTime) {
                        intermission();
                    }

                }

            }
        }
        else{
            if (this.player2.getCardsInHand().size() >= 1){

                if(this.player2.getCardsInHand().get(0).getPrice_bricks()<=this.player2.getPlayerCastle().getBricks() &&
                        this.player2.getCardsInHand().get(0).getPrice_mana()<=this.player2.getPlayerCastle().getMana() &&
                        this.player2.getCardsInHand().get(0).getPrice_weapons()<=this.player2.getPlayerCastle().getWeapons())
                {
                    cardUse(this.player2.getCardsInHand().get(0));
                    this.player2.getPlayerCastle().addBricks(this.player2.getCardsInHand().get(0).getPrice_bricks() * -1);
                    this.player2.getPlayerCastle().addMana(this.player2.getCardsInHand().get(0).getPrice_mana() * -1);
                    this.player2.getPlayerCastle().addWeapons(this.player2.getCardsInHand().get(0).getPrice_weapons() * -1);
                    this.lastCard2.setImageResource(findPictureId(this.player2.getCardsInHand().get(0).getPictureName()));
                    this.player2.getCardsInHand().remove(0);
                    refreshResources();
                    updateWin();
                    cardUsed=true;
                    if(!this.pauseTime) {
                        intermission();
                    }
                }

            }
        }
    }

    public void useCardButton2(){
        if (this.player1Turn) {
            if (this.player1.getCardsInHand().size() >= 2) {
                if(this.player1.getCardsInHand().get(1).getPrice_bricks()<=this.player1.getPlayerCastle().getBricks() &&
                        this.player1.getCardsInHand().get(1).getPrice_mana()<=this.player1.getPlayerCastle().getMana() &&
                        this.player1.getCardsInHand().get(1).getPrice_weapons()<=this.player1.getPlayerCastle().getWeapons())
                {
                    cardUse(this.player1.getCardsInHand().get(1));
                    this.player1.getPlayerCastle().addBricks(this.player1.getCardsInHand().get(1).getPrice_bricks() * -1);
                    this.player1.getPlayerCastle().addMana(this.player1.getCardsInHand().get(1).getPrice_mana() * -1);
                    this.player1.getPlayerCastle().addWeapons(this.player1.getCardsInHand().get(1).getPrice_weapons() * -1);
                    this.lastCard1.setImageResource(findPictureId(this.player1.getCardsInHand().get(1).getPictureName()));
                    this.player1.getCardsInHand().remove(1);
                    refreshHandPlayer();
                    updateWin();
                    if(!this.pauseTime) {
                        intermission();
                    }
                }

            }
        }
        else{
            if (player2.getCardsInHand().size() >= 2){

                if(this.player2.getCardsInHand().get(1).getPrice_bricks()<=this.player2.getPlayerCastle().getBricks() &&
                        this.player2.getCardsInHand().get(1).getPrice_mana()<=this.player2.getPlayerCastle().getMana() &&
                        this.player2.getCardsInHand().get(1).getPrice_weapons()<=this.player2.getPlayerCastle().getWeapons())
                {
                    cardUse(this.player2.getCardsInHand().get(1));
                    this.player2.getPlayerCastle().addBricks(this.player2.getCardsInHand().get(1).getPrice_bricks() * -1);
                    this.player2.getPlayerCastle().addMana(this.player2.getCardsInHand().get(1).getPrice_mana() * -1);
                    this.player2.getPlayerCastle().addWeapons(this.player2.getCardsInHand().get(1).getPrice_weapons() * -1);
                    this.lastCard2.setImageResource(findPictureId(this.player2.getCardsInHand().get(1).getPictureName()));
                    this.player2.getCardsInHand().remove(1);
                    refreshHandPlayer();
                    updateWin();
                    cardUsed=true;
                    if(!this.pauseTime) {
                        intermission();
                    }
                }

            }
        }
    }

    public void useCardButton3(){if (this.player1Turn) {
        if (this.player1.getCardsInHand().size() >= 3) {
            if(this.player1.getCardsInHand().get(2).getPrice_bricks()<=this.player1.getPlayerCastle().getBricks() &&
                    this.player1.getCardsInHand().get(2).getPrice_mana()<=this.player1.getPlayerCastle().getMana() &&
                    this.player1.getCardsInHand().get(2).getPrice_weapons()<=this.player1.getPlayerCastle().getWeapons())
            {
                cardUse(this.player1.getCardsInHand().get(2));
                this.player1.getPlayerCastle().addBricks(this.player1.getCardsInHand().get(2).getPrice_bricks() * -1);
                this.player1.getPlayerCastle().addMana(this.player1.getCardsInHand().get(2).getPrice_mana() * -1);
                this.player1.getPlayerCastle().addWeapons(this.player1.getCardsInHand().get(2).getPrice_weapons() * -1);
                this.lastCard1.setImageResource(findPictureId(this.player1.getCardsInHand().get(2).getPictureName()));
                this.player1.getCardsInHand().remove(2);
                refreshHandPlayer();
                updateWin();
                if(!this.pauseTime) {
                    intermission();
                };
            }

        }
    }
    else{
        if (this.player2.getCardsInHand().size() >= 3){

            if(this.player2.getCardsInHand().get(2).getPrice_bricks()<=this.player2.getPlayerCastle().getBricks() &&
                    this.player2.getCardsInHand().get(2).getPrice_mana()<=this.player2.getPlayerCastle().getMana() &&
                    this.player2.getCardsInHand().get(2).getPrice_weapons()<=this.player2.getPlayerCastle().getWeapons())
            {
                cardUse(this.player2.getCardsInHand().get(2));
                this.player2.getPlayerCastle().addBricks(this.player2.getCardsInHand().get(2).getPrice_bricks() * -1);
                this.player2.getPlayerCastle().addMana(this.player2.getCardsInHand().get(2).getPrice_mana() * -1);
                this.player2.getPlayerCastle().addWeapons(this.player2.getCardsInHand().get(2).getPrice_weapons() * -1);
                this.lastCard2.setImageResource(findPictureId(this.player2.getCardsInHand().get(2).getPictureName()));
                this.player2.getCardsInHand().remove(2);
                refreshHandPlayer();
                updateWin();
                cardUsed=true;
                if(!this.pauseTime) {
                    intermission();
                }
            }

        }
    }
    }

    public void useCardButton4(){
        if (this.player1Turn) {
            if (this.player1.getCardsInHand().size() >= 4) {
                if(this.player1.getCardsInHand().get(3).getPrice_bricks()<=this.player1.getPlayerCastle().getBricks() &&
                        this.player1.getCardsInHand().get(3).getPrice_mana()<=this.player1.getPlayerCastle().getMana() &&
                        this.player1.getCardsInHand().get(3).getPrice_weapons()<=this.player1.getPlayerCastle().getWeapons())
                {
                    cardUse(this.player1.getCardsInHand().get(3));
                    this.player1.getPlayerCastle().addBricks(this.player1.getCardsInHand().get(3).getPrice_bricks() * -1);
                    this.player1.getPlayerCastle().addMana(this.player1.getCardsInHand().get(3).getPrice_mana() * -1);
                    this.player1.getPlayerCastle().addWeapons(this.player1.getCardsInHand().get(3).getPrice_weapons() * -1);
                    this.lastCard1.setImageResource(findPictureId(this.player1.getCardsInHand().get(3).getPictureName()));
                    this.player1.getCardsInHand().remove(3);
                    refreshHandPlayer();
                    updateWin();
                    if(!this.pauseTime) {
                        intermission();
                    }
                }

            }
        }
        else{
            if (this.player2.getCardsInHand().size() >= 4){

                if(this.player2.getCardsInHand().get(3).getPrice_bricks()<=this.player2.getPlayerCastle().getBricks() &&
                        this.player2.getCardsInHand().get(3).getPrice_mana()<=this.player2.getPlayerCastle().getMana() &&
                        this.player2.getCardsInHand().get(3).getPrice_weapons()<=this.player2.getPlayerCastle().getWeapons())
                {
                    cardUse(this.player2.getCardsInHand().get(3));
                    this.player2.getPlayerCastle().addBricks(this.player2.getCardsInHand().get(3).getPrice_bricks() * -1);
                    this.player2.getPlayerCastle().addMana(this.player2.getCardsInHand().get(3).getPrice_mana() * -1);
                    this.player2.getPlayerCastle().addWeapons(this.player2.getCardsInHand().get(3).getPrice_weapons() * -1);
                    this.lastCard2.setImageResource(findPictureId(this.player2.getCardsInHand().get(3).getPictureName()));
                    this.player2.getCardsInHand().remove(3);
                    refreshHandPlayer();
                    updateWin();
                    cardUsed=true;
                    if(!this.pauseTime) {
                        intermission();
                    }
                }

            }
        }
    }

    public void useCardButton5(){
        if (this.player1Turn) {
            if (this.player1.getCardsInHand().size() >= 5) {
                if(this.player1.getCardsInHand().get(4).getPrice_bricks()<=this.player1.getPlayerCastle().getBricks() &&
                        this.player1.getCardsInHand().get(4).getPrice_mana()<=this.player1.getPlayerCastle().getMana() &&
                        this.player1.getCardsInHand().get(4).getPrice_weapons()<=this.player1.getPlayerCastle().getWeapons())
                {
                    cardUse(this.player1.getCardsInHand().get(4));
                    this.player1.getPlayerCastle().addBricks(this.player1.getCardsInHand().get(4).getPrice_bricks() * -1);
                    this.player1.getPlayerCastle().addMana(this.player1.getCardsInHand().get(4).getPrice_mana() * -1);
                    this.player1.getPlayerCastle().addWeapons(this.player1.getCardsInHand().get(4).getPrice_weapons() * -1);
                    this.lastCard1.setImageResource(findPictureId(this.player1.getCardsInHand().get(4).getPictureName()));
                    this.player1.getCardsInHand().remove(4);
                    refreshHandPlayer();
                    updateWin();
                    cardUsed=true;
                    if(!this.pauseTime) {
                        intermission();
                    }
                }

            }
        }
        else{
            if (this.player2.getCardsInHand().size() >= 5){

                if(this.player2.getCardsInHand().get(4).getPrice_bricks()<=this.player2.getPlayerCastle().getBricks() &&
                        this.player2.getCardsInHand().get(4).getPrice_mana()<=this.player2.getPlayerCastle().getMana() &&
                        this.player2.getCardsInHand().get(4).getPrice_weapons()<=this.player2.getPlayerCastle().getWeapons())
                {
                    cardUse(this.player2.getCardsInHand().get(4));
                    this.player2.getPlayerCastle().addBricks(this.player2.getCardsInHand().get(4).getPrice_bricks() * -1);
                    this.player2.getPlayerCastle().addMana(this.player2.getCardsInHand().get(4).getPrice_mana() * -1);
                    this.player2.getPlayerCastle().addWeapons(this.player2.getCardsInHand().get(4).getPrice_weapons() * -1);
                    this.lastCard2.setImageResource(findPictureId(this.player2.getCardsInHand().get(4).getPictureName()));
                    this.player2.getCardsInHand().remove(4);
                    refreshHandPlayer();
                    updateWin();
                    if(!this.pauseTime) {
                        intermission();
                    }
                }

            }
        }
    }

    public void cardUse(Card card){
        if(this.player1Turn){
            switch (card.getUse()){
                case "Castle":
                    this.player1.getPlayerCastle().addCastleHealth(card.getCalculation());
                    break;
                case "Enemy_castle":
                    if(this.player2.getPlayerCastle().getWallHealth()>=card.getCalculation() * -1){
                        this.player2.getPlayerCastle().addWallHealth(card.getCalculation());
                    }
                    else{
                        this.player2.getPlayerCastle().addCastleHealth(card.getCalculation()+this.player2.getPlayerCastle().getWallHealth());
                        this.player2.getPlayerCastle().setWallHealth(0);
                    }
                    break;
                case "All_resources":
                    if(this.player2.getPlayerCastle().getBricks() >= card.getCalculation()){
                        this.player2.getPlayerCastle().addBricks(card.getCalculation()*-1);
                        this.player1.getPlayerCastle().addBricks(card.getCalculation());
                    }
                    else{
                        this.player1.getPlayerCastle().addBricks(this.player2.getPlayerCastle().getBricks());
                        this.player2.getPlayerCastle().addBricks(this.player2.getPlayerCastle().getBricks()*-1);
                    }
                    if(this.player2.getPlayerCastle().getMana() >= card.getCalculation()){
                        this.player2.getPlayerCastle().addMana(card.getCalculation()*-1);
                        this.player1.getPlayerCastle().addMana(card.getCalculation());
                    }
                    else{
                        this.player1.getPlayerCastle().addMana(this.player2.getPlayerCastle().getMana());
                        this.player2.getPlayerCastle().addMana(this.player2.getPlayerCastle().getMana()*-1);
                    }
                    if(this.player2.getPlayerCastle().getWeapons() >= card.getCalculation()){
                        this.player2.getPlayerCastle().addWeapons(card.getCalculation()*-1);
                        this.player1.getPlayerCastle().addWeapons(card.getCalculation());
                    }
                    else{
                        this.player1.getPlayerCastle().addWeapons(this.player2.getPlayerCastle().getWeapons());
                        this.player2.getPlayerCastle().addWeapons(this.player2.getPlayerCastle().getWeapons()*-1);
                    }
                    break;
                case "Wall":
                    this.player1.getPlayerCastle().addWallHealth(card.getCalculation());
                    break;

            }
        }
        else{
            switch (card.getUse()){
                case "Castle":
                    this.player2.getPlayerCastle().addCastleHealth(card.getCalculation());
                    break;
                case "Enemy_castle":
                    if(this.player1.getPlayerCastle().getWallHealth()>=card.getCalculation()* -1){
                        this.player1.getPlayerCastle().addWallHealth(card.getCalculation());
                    }
                    else{
                        this.player1.getPlayerCastle().addCastleHealth(card.getCalculation()+this.player1.getPlayerCastle().getWallHealth());
                        this.player1.getPlayerCastle().setWallHealth(0);
                    }
                    break;
                case "All_resources":
                    if(this.player1.getPlayerCastle().getBricks() >= card.getCalculation()){
                        this.player1.getPlayerCastle().addBricks(card.getCalculation()*-1);
                        this.player2.getPlayerCastle().addBricks(card.getCalculation());
                    }
                    else{
                        this.player2.getPlayerCastle().addBricks(this.player1.getPlayerCastle().getBricks());
                        this.player1.getPlayerCastle().addBricks(this.player1.getPlayerCastle().getBricks()*-1);
                    }
                    if(this.player1.getPlayerCastle().getMana() >= card.getCalculation()){
                        this.player2.getPlayerCastle().addMana(card.getCalculation()*-1);
                        this.player1.getPlayerCastle().addMana(card.getCalculation());
                    }
                    else{
                        this.player2.getPlayerCastle().addMana(this.player1.getPlayerCastle().getMana());
                        this.player1.getPlayerCastle().addMana(this.player1.getPlayerCastle().getMana()*-1);
                    }
                    if(this.player1.getPlayerCastle().getWeapons() >= card.getCalculation()){
                        this.player1.getPlayerCastle().addWeapons(card.getCalculation()*-1);
                        this.player2.getPlayerCastle().addWeapons(card.getCalculation());
                    }
                    else{
                        this.player2.getPlayerCastle().addWeapons(this.player1.getPlayerCastle().getWeapons());
                        this.player1.getPlayerCastle().addWeapons(this.player1.getPlayerCastle().getWeapons()*-1);
                    }
                    break;
                case "Wall":
                    this.player2.getPlayerCastle().addWallHealth(card.getCalculation());
                    break;

            }
        }
        refreshResources();
    }

    public void intermission(){
        this.intermissionTurn=true;
        this.cardUsed=false;
        this.cardButton1.setImageResource(R.drawable.card_back);
        this.cardButton2.setImageResource(R.drawable.card_back);
        this.cardButton3.setImageResource(R.drawable.card_back);
        this.cardButton4.setImageResource(R.drawable.card_back);
        this.cardButton5.setImageResource(R.drawable.card_back);
        updateTurn();
        turnText = findViewById(R.id.whatTurnText);
        new CountDownTimer(5000, 1000) {

            public void onTick(long millisUntilFinished) {
                turnText.setText("Next turn in: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                nextTurn();
            }
        }.start();
    }

    public void nextTurn(){
        if(this.player1Turn) {
            this.player1Turn = false;
            this.intermissionTurn=false;
            this.player2.getPlayerCastle().addBricks(this.player1.getPlayerCastle().getBuilders());
            this.player2.getPlayerCastle().addMana(this.player1.getPlayerCastle().getMagic());
            this.player2.getPlayerCastle().addWeapons(this.player1.getPlayerCastle().getBlacksmith());
            playerDraw();
            refreshHandPlayer();
            refreshResources();
            updateTurn();
            aiTurn();
        }
        else{
            this.player1Turn=true;
            this.intermissionTurn=false;
            this.player1.getPlayerCastle().addBricks(this.player1.getPlayerCastle().getBuilders());
            this.player1.getPlayerCastle().addMana(this.player1.getPlayerCastle().getMagic());
            this.player1.getPlayerCastle().addWeapons(this.player1.getPlayerCastle().getBlacksmith());
            playerDraw();
            refreshHandPlayer();
            refreshResources();
            updateTurn();
        }
    }

    public void updateTurn(){
        this.turnText=findViewById(R.id.whatTurnText);

        if(this.intermissionTurn){
            this.turnText.setText("Intermission");
        }
        else{
            if(this.player1Turn){
                this.turnText.setText("Player 1 Turn");
            }
            else{
                this.turnText.setText("Player 2 Turn");
            }
        }
    }
    public void updateWin(){
        if (player1Turn) {
            if(player1.getPlayerCastle().getCastleHealth()>=100 || player2.getPlayerCastle().getCastleHealth()<=0){
                this.winScreen.setVisibility(View.VISIBLE);
                this.winScreen.setImageResource(R.drawable.item_victory);
                this.pauseTime=true;
            }
        }
        else{
            if(player2.getPlayerCastle().getCastleHealth()>=100 || player1.getPlayerCastle().getCastleHealth()<=0){
                this.winScreen.setVisibility(View.VISIBLE);
                this.winScreen.setImageResource(R.drawable.item_lose);
                this.pauseTime=true;
            }
        }
    }

    public void aiTurn(){
        for(int i=0; i < this.player2.getCardsInHand().size();i++){
            if(i==0){
                useCardButton1();
            }
            else if(i==1){
                useCardButton2();
            }
            else if(i==2){
                useCardButton3();
            }
            else if(i==3){
                useCardButton4();
            }
            else if(i==4){
                useCardButton5();
            }
            else{
                intermission();
                break;
            }
        }
        if(!cardUsed){
            intermission();
        }
    }
}
