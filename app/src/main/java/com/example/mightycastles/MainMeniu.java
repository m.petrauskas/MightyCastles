package com.example.mightycastles;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.net.URL;

public class MainMeniu extends Activity {
    private Button menuPlayButton;
    private Button menuOptionsButton;
    private Button easterEgg;
    private Button dataBase;
    public static MediaPlayer ring;
    private ImageView meme;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.ring= MediaPlayer.create(MainMeniu.this,R.raw.menu_theme_new);
        this.ring.setLooping(true);
        this.ring.start();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_meniu);
        dataBase = findViewById(R.id.databaseButton);
        this.meme = findViewById(R.id.memePic);
        Picasso.with(this).load("https://www.neogaf.com/data/avatars/m/10/10994.jpg?1548793742").into(this.meme);
        //this.meme.setImageDrawable(LoadImageFromWebOperations("https://www.neogaf.com/data/avatars/m/10/10994.jpg?1548793742"));

        menuPlayButton = (Button) findViewById(R.id.playButton);
        menuPlayButton.setOnClickListener(new View.OnClickListener(){@Override
        public void onClick(View v) {
            openPlayActivity();
        }
        });
        this.easterEgg = findViewById(R.id.easterEggButton);
        this.easterEgg.setBackgroundColor(Color.TRANSPARENT);
        this.easterEgg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                meme.setVisibility(View.VISIBLE);
            }
        });
        this.menuOptionsButton= findViewById(R.id.optionButtno);
        this.menuOptionsButton.setOnClickListener(new View.OnClickListener(){@Override
        public void onClick(View v) {
            openOptionActivity();
        }
        });
        dataBase.setOnClickListener(new View.OnClickListener(){@Override
        public void onClick(View v) {
            openDatabaseActivity();
        }
        });
    }
    public void openPlayActivity(){
        Intent intent = new Intent(this, Play.class);
        startActivity(intent);
    }
    public void openOptionActivity(){
        Intent intent = new Intent(this, Options.class);
        startActivity(intent);
    }

    public void openDatabaseActivity(){
        Intent intent = new Intent(this, DataBase.class);
        startActivity(intent);
    }
}
