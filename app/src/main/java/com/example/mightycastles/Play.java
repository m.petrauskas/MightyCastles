package com.example.mightycastles;//

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import com.example.mightycastles.mightycastles.classes.SQLiteTest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Play extends Activity {
    private Button playGame;
    private Button playGameBot;
    private RadioButton easy;
    private RadioButton medium;
    private RadioButton hard;
    private RadioButton chroma1Player1;
    private RadioButton chroma2Player1;
    private RadioButton chroma3Player1;
    private RadioButton chroma1Player2;
    private RadioButton chroma2Player2;
    private RadioButton chroma3Player2;
    public EditText player1Name;
    public EditText player2Name;
    public static String player1Nametext;
    public static String player2Nametext;
    public static String difficulty;
    public static String player1Chroma;
    public static String player2Chroma;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        player1Name = findViewById(R.id.player1NameText);
        player2Name = findViewById(R.id.player2NameText);
        player1Nametext = player1Name.getText().toString();
        player2Nametext = player2Name.getText().toString();


        playGame = (Button) findViewById(R.id.playGameButton);
        playGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(chroma1Player1.isChecked()){
                    player1Chroma="FirstChroma";
                }
                else if(chroma2Player1.isChecked()){
                    player1Chroma="SecondChroma";
                }
                else{
                    player1Chroma="LastChroma";
                }
                if(chroma1Player2.isChecked()){
                    player2Chroma="FirstChroma";
                }
                else if(chroma2Player2.isChecked()){
                    player2Chroma="SecondChroma";
                }
                else{
                    player2Chroma="LastChroma";
                }
                openGameplayActivity();
            }
        });
        easy = findViewById(R.id.easyBotChose);
        medium = findViewById(R.id.mediumBotChose);
        hard = findViewById(R.id.hardBotChose);
        chroma1Player1 = findViewById(R.id.chroma1Player1);
        chroma2Player1 = findViewById(R.id.chroma2Player1);
        chroma3Player1 = findViewById(R.id.chroma3Player1);
        chroma1Player2 = findViewById(R.id.chroma1Player2);
        chroma2Player2 = findViewById(R.id.chroma2Player2);
        chroma3Player2 = findViewById(R.id.chroma3Player2);
        playGameBot = (Button) findViewById(R.id.playVsBots);
        playGameBot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(easy.isChecked()){
                    if(chroma1Player1.isChecked()){
                        player1Chroma="FirstChroma";
                    }
                    else if(chroma2Player1.isChecked()){
                        player1Chroma="SecondChroma";
                    }
                    else{
                        player1Chroma="LastChroma";
                    }
                    if(chroma1Player2.isChecked()){
                        player2Chroma="FirstChroma";
                    }
                    else if(chroma2Player2.isChecked()){
                        player2Chroma="SecondChroma";
                    }
                    else{
                        player2Chroma="LastChroma";
                    }
                    openGameplayWithBotsActivity("Easy");
                }
                else if(medium.isChecked()){
                    if(chroma1Player1.isChecked()){
                        player1Chroma="FirstChroma";
                    }
                    else if(chroma2Player1.isChecked()){
                        player1Chroma="SecondChroma";
                    }
                    else{
                        player1Chroma="LastChroma";
                    }
                    if(chroma1Player2.isChecked()){
                        player2Chroma="FirstChroma";
                    }
                    else if(chroma2Player2.isChecked()){
                        player2Chroma="SecondChroma";
                    }
                    else{
                        player2Chroma="LastChroma";
                    }
                    openGameplayWithBotsActivity("Medium");
                }
                else if(hard.isChecked()){
                    if(chroma1Player1.isChecked()){
                        player1Chroma="FirstChroma";
                    }
                    else if(chroma2Player1.isChecked()){
                        player1Chroma="SecondChroma";
                    }
                    else{
                        player1Chroma="LastChroma";
                    }
                    if(chroma1Player2.isChecked()){
                        player2Chroma="FirstChroma";
                    }
                    else if(chroma2Player2.isChecked()){
                        player2Chroma="SecondChroma";
                    }
                    else{
                        player2Chroma="LastChroma";
                    }
                    openGameplayWithBotsActivity("Hard");
                }
            }
        });
    }
    public void openGameplayActivity(){
        /*try {
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            String url = "jdbc:mysql://remotemysql.com:3306/VWiM5yBTiS";
            Connection conn = DriverManager.getConnection(url,"VWiM5yBTiS","7vuEKvEDlz");
            Statement st = conn.createStatement();
            st.executeUpdate("INSERT INTO usernames VALUES (name, \"vardas\")");
            conn.close();
        } catch (Exception e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }

        player1Nametext = player1Name.getText().toString();
        player2Nametext = player2Name.getText().toString();
*/
        Intent intent = new Intent(this, Gameplay.class);
        startActivity(intent);
    }
    public void openGameplayWithBotsActivity(String difficulty){
        player1Nametext = player1Name.getText().toString();
        player2Nametext = player2Name.getText().toString();
        Intent intent = new Intent(this, GameplayWithBots.class);
        this.difficulty = difficulty;
        startActivity(intent);
    }
}

